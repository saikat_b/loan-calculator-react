import React, { Component } from 'react';
import Slider from 'react-rangeslider';
import './App.css';
import 'react-rangeslider/lib/index.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      amountValue: 2000,
      monthsValue: 10,
      data: [],
    }
  this.handleAmountChange = this.handleAmountChange.bind(this);
  this.handleMonthsChange = this.handleMonthsChange.bind(this);
  this.callAPI = this.callAPI.bind(this);
  }

  componentWillMount() {
    this.callAPI();
  }

  handleAmountChange = value => {
    this.setState({amountValue: value});
    this.callAPI();
  };

  handleMonthsChange = value => {
    this.setState({monthsValue: value});
    this.callAPI();
  };

  callAPI(){
     fetch(`https://ftl-frontend-test.herokuapp.com/interest?amount=${this.state.amountValue}&numMonths=${this.state.monthsValue}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            data: result.data,
            ROI: result.interestRate, 
            EMI: result.monthlyPayment.amount,
            currency: result.monthlyPayment.currency,
          });
        },
      )
  }

  render() {
    let {amountValue, monthsValue} = this.state;
    return (
      <div className="App">

        <span className="Title">LOAN CALCULATOR</span>

        <h4>Loan Amount = INR {amountValue}</h4>
        <Slider
          step = {100}
          max = {5000}
          min = {500}
          value = {amountValue}
          onChange = {this.handleAmountChange}
          />

          <h4>Months = {monthsValue} months</h4>

          <Slider
          step = {1}
          max = {24}
          min = {6}
          value = {monthsValue}
          onChange = {this.handleMonthsChange}
          />

          <div className="flex">
            <h4>ROI= { this.state.ROI*100 }%</h4>
            <h4>EMI= { this.state.currency } { this.state.EMI } </h4>
          </div>
      </div>
    );
  }
}

export default App;
